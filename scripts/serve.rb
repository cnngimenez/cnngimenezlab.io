# Copyright 2023 Christian Gimenez
#
# serve.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'haml'
require 'sinatra'

configure do
  set :views, [ '.' ]
end

get '/' do
  puts 'Rule: /'
  haml :index
end

get /.*/ do
  puts 'Rule /.*/'
  path = :index if path == '/'
  
  path = request.path_info
  extension = File.extname path
 
  if extension == '.haml'
    puts "haml #{path}"
    haml path
  else
    puts "send_file #{path}"
    send_file "./#{path}"
  end
end
