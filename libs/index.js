/* 
   Copyright 2023 Christian Gimenez
   
   Author: Christian Gimenez   

   index.js
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function init_aos() {
    window.aos_i = AOS.init({
        disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
        startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
        // initClassName: 'aos-init', // class applied after initialization
        // animatedClassName: 'aos-animate', // class applied on animation
        useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
        disableMutationObserver: false, // disables automatic mutations' detections (advanced)
        debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
        throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

        // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
        offset: 250, // offset (in px) from the original trigger point
        delay: 0, // values from 0 to 3000, with step 50ms
        duration: 400, // values from 0 to 3000, with step 50ms
        easing: 'ease', // default easing for AOS animations
        once: false, // whether animation should happen only once - while scrolling down
        mirror: false, // whether elements should animate out while scrolling past them
        anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
    });
    console.log('AOS initialized');
}

$(document).ready(function(){
    
    var masonry_i = new Masonry( '.row', {
        percentPosition: true,
        gutter: 0,
        // columnWidth: '.col-lg-1',
        originLeft: true,
        originTop: true,
        horizontalOrder: false        
    });
    masonry_i.on('layoutComplete', init_aos);

    window.masonry_i = masonry_i;
    
    init_aos();

    var url = new URL(document.URL);
    if (url.searchParams.get('particles') != 'off'){
        particlesJS.load('particles-title', 'libs/particles-config.json', function() {
            console.log('callback - particles.js config loaded');
        });
    }

    console.log('index.js: Initialization ended');
});
